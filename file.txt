use a databaseTabble

->use contractManagement

insert data

-> db.purchase_orders.insert({"Client_Name" : "Tanmay", "Project_Name" :"Bedi solutions", "client_Sponser" : ["abc","xyz"], Client_Finance_Controller : ["pqr","stu"], Targetted_resources : ["mnb","jhg"], status: "Pending", "type" : "PO", PO_Number : "uigubjtnhvnohk78", PO_Amount : 5878678, Currency : "INR", Document_Name : "cgubcjgmni", Document_Type : ".pdf", Remarks : "crbghjkbnvntklvvv" } )
-> db.purchase_orders.insert({"Client_Name" : "saurabh", "Project_Name" :"Bedi plus", "client_Sponser" : ["abc"], Client_Finance_Controller : ["pqr","stu","mno"], Targetted_resources : ["mnb","jhg","bfhgbj","kgngvjbij"], status: "Rejected", "type" : "SOW", PO_Number : "umnxsd3456", PO_Amount : 67000, Currency : "USD", Document_Name : "bvuhbb kjfcjbijtvt", Document_Type : ".xml", Remarks : "crbghjtncjngviombjkbnvntklvvv" } )
-> db.purchase_orders.insert({"Client_Name" : "lokesh", "Project_Name" :"contract management", "client_Sponser" : ["mno","xyz"], Client_Finance_Controller : ["bbf","ssdf"], Targetted_resources : ["klp"], status: "Accepted", "type" : "PO", PO_Number : "hjdpi1245", PO_Amount : 200000, Currency : "Usd", Document_Name : "cgubcjgmcrgnvknlkvni", Document_Type : ".pdf", Remarks : "crtornyoonv jknvjntehvtklvvv" } )
-> db.purchase_orders.insert({"Client_Name" : "vikash", "Project_Name" :"PMO", "client_Sponser" : ["zzz","yyy"], Client_Finance_Controller : ["aaaa","bbbb","llp"], Targetted_resources : ["mnb","jhg","tto","ggf"], status: "Accepted", "type" : "PO", PO_Number : "utlp4567", PO_Amount : 300000, Currency : "INR", Document_Name : "cgcrgnvlnlknhubcjgmni", Document_Type : ".pdf", Remarks : "jcbgigcjbvihrintnnhivh" } )

read the data
-> db.purchase_orders.find().forEach(printjson)
-> db.purchase_orders.find({Client_Name: "Tanmay"}).forEach(printjson)

update data
-> db.purchase_orders.update({"Id" : 1},
                                {$set: {"Type": "SOW", "PO_Number": "ttyyple8976"}}
                            )
-> db.purchase_orders.update({"Id" : 2},
                                {$set: {"Status": "Accepted"}}
                            )
-> db.purchase_orders.update({"Id" : 3},
                                {$set: {"Type": "SOW", "PO_Amount": "80000", "Status" : "Pending"}}
                            )