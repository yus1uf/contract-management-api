const chai = require("chai");
const expect = chai.expect;
const EmployeesService = require("../api/service/EmployeeService")
const sinon = require("sinon");
const EmployeeController = require("../api/controllers/EmployeeController")

const stubValue = {
    _id:"24345df345",
    Project_Id: "VBPR78",
    Employee_Id: "VBEMP35",
    Employee_Name:"Yusuf Shekh",
    Allocation_Rate:70,
    Start_Date: "02/15/2021",
    End_Date: "06/23/2021"
};

describe("Employee Controller Call", function() {

    describe("Store Details",function(){
        let req;
        let res;
        let objService;
        beforeEach(() => {
          req = { body:stubValue };
          res = { json: function() {} }
      });
      it("Should Store details",async function(done){
        const mock = sinon.mock(res);
        const status = sinon.spy();
      mock
        .expects("json")
        .once()
        .withExactArgs({ data: stubValue });
        objService = new EmployeesService();
      const stub = sinon.stub(objService, "storeService").returns(stubValue);
      done();
      const  empControllerObj = new EmployeeController();
      const emp = await empControllerObj.storeEmployee(req,res);
      expect(stub.calledOnce).to.be.true;
      expect(status.args).to.equal(201);
      expect(json.calledOnce).to.be.true;
      mock.verify();
      })
      })

 describe("Update Employee Details",function(){
    let req;
    let res;
    let objService;
    beforeEach(() => {
      req = { params: { _id: stubValue._id },body:stubValue };
      res = { json: function() {} }
  });
  it("Should Update details",async function(done){
    const mock = sinon.mock(res);
  mock
    .expects("json")
    .once()
    .withExactArgs({ data: stubValue });
    objService = new EmployeesService();
  const stub = sinon.stub(objService, "updateService").returns(stubValue);
  done();
  const  empControllerObj = new EmployeeController();
  const updateDetails = await empControllerObj.updateAssigneeDetails(req,res);
  expect(stub.calledOnce).to.be.true;
  mock.verify();
  })
 })

 describe("Unassign Employee",function(){
    let req;
    let res;
    let objService;
    beforeEach(() => {
      req = { params: { _id: stubValue._id },body:stubValue };
      res = { json: function() {} }
  });
  it("Should Unassign employee",async function(done){
    const mock = sinon.mock(res);
  mock
    .expects("json")
    .once()
    .withExactArgs({ data: stubValue });
    objService = new EmployeesService();
  const stub = sinon.stub(objService, "unassignService").returns(stubValue);
  done();
  const  empControllerObj = new EmployeeController();
  const unassign = await empControllerObj.unassign(req,res);
  expect(stub.calledOnce).to.be.true;
  mock.verify();
  })
  
 });
});