const chai = require("chai");
const expect = chai.expect;
const EmployeesModel = require("../api/models/EmployeesModel");
const sinon = require("sinon");
const EmployeeCrud = require("../api/DAO/EmployeeCrud");

const stubValue = {
    _id:"24345df345",
    Project_Id: "VBPR78",
    Employee_Id: "VBEMP35",
    Employee_Name:"Yusuf Shekh",
    Allocation_Rate:70,
    Start_Date: "02/15/2021",
    End_Date: "06/23/2021"
};

describe("Employee DAO Calling", function(){

    describe("Store Assignee Details",function(){
        let stub
        before(()=>{})
        it("Should Store the Asignee Details",async function(){
            const empObject = new EmployeeCrud()
            const details = empObject.storeAssignee(stubValue)
        });
        after(()=>{})
    });

    describe("Update Assignee Details",function(){
        let stub
        before(()=>{})
        it("Should Update the Asignee Details",async function(){
            stub = sinon.stub(EmployeesModel,"updateOne").returns(stubValue)
            const empObject = new EmployeeCrud()
            const details = empObject.updateAssigneeDetails(stubValue._id,stubValue)
            expect(stub.calledOnce).to.be.true;
            stub.restore()
        });
        after(()=>{})
    });

    describe("Unassign Employee",function(){
        let stub
        before(()=>{})
        it("Should Unassign the Asignee",async function(){
            stub = sinon.stub(EmployeesModel,"updateOne").returns(stubValue)
            const empObject = new EmployeeCrud()
            const details = empObject.unassignEmployee(stubValue._id,"12/23/2021")
            expect(stub.calledOnce).to.be.true;
            stub.restore()
        });
        after(()=>{})
    })
})