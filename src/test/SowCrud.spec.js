const chai = require("chai");
const expect = chai.expect;
const purchaseOrderModel = require("../api/models/POModels");
const sinon = require("sinon");
const SowCrud = require("../api/DAO/sowCrud");

const stubValue = {
  id: "347",
  Client_Name: "Yusuf Shekh",
  Project_Name: "SSW Solution",
  Client_Sponser: ["NNM", "VDT"],
  Client_Finance_Controller: ["SAW", "QSE"],
  Targetted_Resources: ["BND", "NJY"],
  Status: "Rejected",
  Type: "PO",
  PO_Number: "12ndh3",
  PO_Amount: 1224,
  Currency: "INR",
  Document_Name: "SSWdoc",
  Document_Type: ".pdf",
  Remarks: "sdfhskjhdjv",
};
describe("PO/SOW DAO Calling", function () {
  this.beforeEach(function () {
    const stub = sinon.stub();
    stub.resetHistory();
  });

  describe("Get Details", function () {
   let stub
   before(()=>{})
    it("Should Retrieve all records from database", async function () {
      const stub = sinon.stub(purchaseOrderModel, "find").returns(stubValue);
      const SowCrudOb = new SowCrud();
      const details = await SowCrudOb.getPoDetails();

      expect(stub.calledOnce).to.be.true;
      expect(details.Client_Name).to.equal(stubValue.Client_Name);
      expect(details.Project_Name).to.equal(stubValue.Project_Name);
      expect(details.Client_Sponser).to.eql(stubValue.Client_Sponser);
      expect(details.Client_Finance_Controller).to.eql(
        stubValue.Client_Finance_Controller
      );
      expect(details.Targetted_Resources).to.eql(stubValue.Targetted_Resources);
      expect(details.Status).to.equal(stubValue.Status);
      expect(details.PO_Number).to.equal(stubValue.PO_Number);
      expect(details.PO_Amount).to.equal(stubValue.PO_Amount);
      expect(details.Currency).to.equal(stubValue.Currency);
      expect(details.Document_Name).to.equal(stubValue.Document_Name);
      expect(details.Document_Type).to.equal(stubValue.Document_Type);
      expect(details.Remarks).to.equal(stubValue.Remarks);
      stub.restore()
    });
    after(()=>{})
  });

  describe("Get Details By Id", function () {
    it("Should Retrieve the records based on specific id", async function () {
      const stub = sinon.stub(purchaseOrderModel, "findOne").returns(stubValue);
      const SowCrudOb = new SowCrud();
      const details = await SowCrudOb.getPODetailsByID(stubValue.id);

      expect(stub.calledOnce).to.be.true;
      expect(details.id).to.equal(stubValue.id);
      expect(details.Client_Name).to.equal(stubValue.Client_Name);
      expect(details.Project_Name).to.equal(stubValue.Project_Name);
      expect(details.Client_Sponser).to.eql(stubValue.Client_Sponser);
      expect(details.Client_Finance_Controller).to.eql(
        stubValue.Client_Finance_Controller
      );
      expect(details.Targetted_Resources).to.eql(stubValue.Targetted_Resources);
      expect(details.Status).to.equal(stubValue.Status);
      expect(details.PO_Number).to.equal(stubValue.PO_Number);
      expect(details.PO_Amount).to.equal(stubValue.PO_Amount);
      expect(details.Currency).to.equal(stubValue.Currency);
      expect(details.Document_Name).to.equal(stubValue.Document_Name);
      expect(details.Document_Type).to.equal(stubValue.Document_Type);
      expect(details.Remarks).to.equal(stubValue.Remarks);
    });
  });

  describe("Update Details By Id", function () {
    let stub
   before(()=>{})
    it("Should Update the records bassed on specific id", async function () {
       stub = sinon
        .stub(purchaseOrderModel, "updateOne")
        .returns(stubValue);
      const SowCrudOb = new SowCrud();
      const details = await SowCrudOb.updatePoDetails(stubValue.id, stubValue);
      expect(stub.calledOnce).to.be.true;
      stub.restore()
    });
    after(()=>{})
  });

    describe("Sort Details", function(){
        let stub
      it("Should Sort the records based on params", async function(){

         stub = sinon.stub(purchaseOrderModel,"find").returns( sort=> {
          return stubValue;
      });
        const SowCrudOb = new SowCrud();
        const details = await SowCrudOb.sort("Client_Name")
        expect(stub.calledOnce).to.be.true;
      })
    })

    describe("Update Status By Id", function () {
      it("Should find the record of specific id", async function () {
        const stub = sinon
          .stub(purchaseOrderModel, "findById")
          .returns(stubValue);
        const SowCrudOb = new SowCrud();
        const details = await SowCrudOb.updatePoStatus(stubValue.id);
        expect(stub.calledOnce).to.be.true;
      });
      it("Should update the status of specific id", async function () {
        const stub = sinon
          .stub(purchaseOrderModel, "updateOne")
          .returns(stubValue);
        const SowCrudOb = new SowCrud();
        const details = await SowCrudOb.updatePoStatus("drafted");
        expect(details.message).to.be.string;
      });
    });
});

