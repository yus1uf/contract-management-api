const chai = require("chai");
const expect = chai.expect;
const EmployeesService = require("../api/service/EmployeeService")
const sinon = require("sinon");
const EmployeeCrud = require("../api/DAO/EmployeeCrud");

const stubValue = {
    _id:"24345df345",
    Project_Id: "VBPR78",
    Employee_Id: "VBEMP35",
    Employee_Name:"Yusuf Shekh",
    Allocation_Rate:70,
    Start_Date: "02/15/2021",
    End_Date: "06/23/2021"
};

describe("Employee Service Call",function(){
    describe("call Store Assignee",function(){
        let stub
        before(()=>{})
       it("Should Store Assignee Details",async function(){
         stub = sinon
        .stub(EmployeeCrud.prototype, "storeAssignee")
        .returns(stubValue);
      const serviceObject = new EmployeesService();
      const details = await serviceObject.storeService(stubValue);
      expect(stub.calledOnce).to.be.true;
      stub.restore()
       })
       after(()=>{})
    })

    describe("Update Assignee Details",function(){
        let stub
        before(()=>{})
       it("Should Update Assignee Details",async function(){
         stub = sinon
        .stub(EmployeeCrud.prototype, "updateAssigneeDetails")
        .returns(stubValue);
      const serviceObject = new EmployeesService();
      const details = await serviceObject.updateService(stubValue._id,stubValue);
      expect(stub.calledOnce).to.be.true;
      stub.restore()
       })
       after(()=>{})
    })

    describe("Unassign Employee",function(){
        let stub
        before(()=>{})
       it("Should Unassign Employee",async function(){
         stub = sinon
        .stub(EmployeeCrud.prototype, "unassignEmployee")
        .returns(stubValue);
      const serviceObject = new EmployeesService();
      const details = await serviceObject.unassignService(stubValue._id);
      expect(stub.calledOnce).to.be.true;
      stub.restore()
       })
       after(()=>{})
    })
})