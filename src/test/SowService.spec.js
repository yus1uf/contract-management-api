const chai = require("chai");
const expect = chai.expect;
const SowService = require("../api/service/sowService");
const sinon = require("sinon");
const SowCrud = require("../api/DAO/sowCrud");
const stubValue = {
  id: "347",
  Client_Name: "Yusuf Shekh",
  Project_Name: "SSW Solution",
  Client_Sponser: ["NNM", "VDT"],
  Client_Finance_Controller: ["SAW", "QSE"],
  Targetted_Resources: ["BND", "NJY"],
  Status: "Rejected",
  Type: "PO",
  PO_Number: "12ndh3",
  PO_Amount: 1224,
  Currency: "INR",
  Document_Name: "SSWdoc",
  Document_Type: ".pdf",
  Remarks: "sdfhskjhdjv",
};
describe("Service Call", function () {
  describe("Get Details", function () {
    it("Should Return  all records", async function () {
      const serviceObject = new SowService();
      const stub = sinon
        .stub(SowCrud.prototype, "getPoDetails")
        .returns(stubValue);
      const details = await serviceObject.details();
      expect(stub.calledOnce).to.be.true;
      expect(details.Client_Name).to.equal(stubValue.Client_Name);
      expect(details.Project_Name).to.equal(stubValue.Project_Name);
      expect(details.Client_Sponser).to.eql(stubValue.Client_Sponser);
    });
  });

  describe("Get ID details", function () {
    it("Should Return specific ID details", async function () {
      const serviceObject = new SowService();
      const stub = sinon
        .stub(SowCrud.prototype, "getPODetailsByID")
        .returns(stubValue);
      const details = await serviceObject.callGetDetailsById(stubValue.id);
      expect(stub.calledOnce).to.be.true;
      expect(details.id).to.be.equal(stubValue.id);
    });
  });

  describe("Update Details By Id", function () {
    it("Should Update the records bassed on specific id", async function () {
      const stub = sinon
        .stub(SowCrud.prototype, "updatePoDetails")
        .returns(stubValue);
      const serviceObject = new SowService();
      const details = await serviceObject.callUpdateDetails(
        stubValue.id,
        stubValue
      );
      expect(stub.calledOnce).to.be.true;
    });
  });

  describe("Sort Details", function () {
    it("Should Sort the records bassed on specific Param", async function () {
      const stub = sinon.stub(SowCrud.prototype, "sort").returns(stubValue);
      const serviceObject = new SowService();
      const details = await serviceObject.sortSOW("Client_Name");
      expect(stub.calledOnce).to.be.true;
    });
  });

  describe("Store Details", function () {
    it("Should Store the records", async function () {
      const stub = sinon
        .stub(SowCrud.prototype, "storeDetails")
        .returns(stubValue);
      const serviceObject = new SowService();
      const details = await serviceObject.callStoreDetails(stubValue);
      expect(stub.calledOnce).to.be.true;
      expect(details.Client_Name).to.equal(stubValue.Client_Name);
      expect(details.Project_Name).to.equal(stubValue.Project_Name);
      expect(details.Client_Sponser).to.eql(stubValue.Client_Sponser);
      expect(details.Client_Finance_Controller).to.eql(
        stubValue.Client_Finance_Controller
      );
      expect(details.Targetted_Resources).to.eql(stubValue.Targetted_Resources);
      expect(details.Status).to.equal(stubValue.Status);
      expect(details.PO_Number).to.equal(stubValue.PO_Number);
      expect(details.PO_Amount).to.equal(stubValue.PO_Amount);
      expect(details.Currency).to.equal(stubValue.Currency);
      expect(details.Document_Name).to.equal(stubValue.Document_Name);
      expect(details.Document_Type).to.equal(stubValue.Document_Type);
      expect(details.Remarks).to.equal(stubValue.Remarks);
    });
  });
});
