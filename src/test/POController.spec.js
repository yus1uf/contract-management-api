const chai = require("chai")
const expect = chai.expect;
const SowService = require("../api/service/sowService")
const sinon = require("sinon");
const POController = require("../api/controllers/POController")
const stubValue = {
    _id:"347",
    Client_Name: "Yusuf Shekh",
    Project_Name: "SSW Solution",
    Client_Sponser: ["NNM","VDT"],
    Client_Finance_Controller: ["SAW","QSE"],
    Targetted_Resources: ["BND","NJY"],
    Status: "Rejected",
    Type: "PO",
    PO_Number: "12ndh3",
    PO_Amount: 1224,
    Currency: "INR",
    Document_Name: "SSWdoc",
    Document_Type:".pdf",
    Remarks: "sdfhskjhdjv"
  }

  describe(" Controller Call",function(){
      describe("Get Details By ID",function(){
        let req;
        let res;
        let objService;
        beforeEach(() => {
          req = { params: { _id: stubValue.id } };
          res = { json: function() {} }
      });
      it("Should Return details matched with param",async function(done){
        const mock = sinon.mock(res);
      mock
        .expects("json")
        .once()
        .withExactArgs({ data: stubValue });
        objService = new SowService();
      const stub = sinon.stub(objService, "callGetDetailsById").returns(stubValue);
      done();
     const  userControllerObj = new POController();
     const user = await userControllerObj.getDetailsById(req,res);
      expect(stub.calledOnce).to.be.true;
      mock.verify();
      })
      })


      describe("Get All Details",function(){
        let req;
        let res;
        let objService;
        beforeEach(() => {
          req = { params: { _id: stubValue.id } };
          res = { json: function() {} }
      });
      it("Should Return All details",async function(done){
        const mock = sinon.mock(res);
      mock
        .expects("json")
        .once()
        .withExactArgs({ data: stubValue });
        objService = new SowService();
      const stub = sinon.stub(objService, "details").returns(stubValue);
      done();
     const  userControllerObj = new POController();
      const user = await userControllerObj.getPODetais(req,res);
      expect(stub.calledOnce).to.be.true;
      mock.verify();
      })
      })


      describe("Update Details",function(){
        let req;
        let res;
        let objService;
        beforeEach(() => {
          req = { params: { _id: stubValue.id },body:stubValue };
          res = { json: function() {} }
      });
      it("Should Update details",async function(done){
        const mock = sinon.mock(res);
      mock
        .expects("json")
        .once()
        .withExactArgs({ data: stubValue });
        objService = new SowService();
      const stub = sinon.stub(objService, "callUpdateDetails").returns(stubValue);
      done();
      const  userControllerObj = new POController();
      const user = await userControllerObj.updatePODetais(req,res);
      expect(stub.calledOnce).to.be.true;
      mock.verify();
      })
      })


      describe("Store Details",function(){
        let req;
        let res;
        let objService;
        beforeEach(() => {
          req = { body:stubValue };
          res = { json: function() {} }
      });
      it("Should Store details",async function(done){
        const mock = sinon.mock(res);
        const status = sinon.spy();
      mock
        .expects("json")
        .once()
        .withExactArgs({ data: stubValue });
        objService = new SowService();
      const stub = sinon.stub(objService, "callStoreDetails").returns(stubValue);
      done();
      const  userControllerObj = new POController();
      const user = await userControllerObj.storePODetails(req,res);
      expect(stub.calledOnce).to.be.true;
      expect(status.args).to.equal(201);
      expect(json.calledOnce).to.be.true;
      mock.verify();
      })
      })
    })
  