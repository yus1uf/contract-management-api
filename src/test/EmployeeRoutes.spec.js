const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
chai.use(chaiHttp);

const Router = require("../api/routs/EmployeeRoutes");

describe("Employee Router Calling",function(){
    describe("post api", function () {
        it("save Assignee", function () {
          chai
            .request(Router)
            .post("/addAssignee")
            .end((error, response) => {
              expect(response.status).to.be.equal(201);
              done();
            });
        });
    
        it("failed to save po details", function () {
          chai
            .request(Router)
            .post("/addAssignee")
            .end((error, response) => {
              expect(response.status).to.be.equal(400);
              done();
            });
        });
      });

      describe("update Asignee Details", function () {
        it("should update Asignee details by id ", function () {
          chai
            .request(Router)
            .patch("/updateDetails/:id")
            .end((error, response) => {
              expect(response.status).to.be.equal(200);
              done();
            });
        });
    
        it("failed to update po details", function () {
          chai
            .request(Router)
            .patch("/updateDetails/:id")
            .end((error, response) => {
              expect(response.status).to.be.equal(400);
              done();
            });
        });
      });
});