const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect;
chai.use(chaiHttp);

const Router = require("../api/routs/PORoutes");

describe("Router calling", function () {
  describe("Get api", function () {
    it("get po", function () {
      chai
        .request(Router)
        .get("/getPoDetails")
        .end((error, response) => {
          expect(response.status).to.be.equal(200);
          done();
        });
    });

    it("failed to get po details", function () {
      chai
        .request(Router)
        .get("/getPoDetails")
        .end((error, response) => {
          expect(response.status).to.be.equal(400);
          done();
        });
    });
  });

  describe("post api", function () {
    it("save po", function () {
      chai
        .request(Router)
        .post("/savePoDetails")
        .end((error, response) => {
          expect(response.status).to.be.equal(201);
          done();
        });
    });

    it("failed to save po details", function () {
      chai
        .request(Router)
        .post("/savePoDetails")
        .end((error, response) => {
          expect(response.status).to.be.equal(400);
          done();
        });
    });
  });

  describe("Get api by id", function () {
    it("get po details by id", function () {
      chai
        .request(Router)
        .get("/getPoDetailsByID/:id")
        .end((error, response) => {
          expect(response.status).to.be.equal(200);
          done();
        });
    });

    it("failed to get po details by id", function () {
      chai
        .request(Router)
        .get("/getPoDetailsByID/:id")
        .end((error, response) => {
          expect(response.status).to.be.equal(400);
          done();
        });
    });
  });

  describe("update api", function () {
    it("update po details by id ", function () {
      chai
        .request(Router)
        .patch("/updatePoDetails/:id")
        .end((error, response) => {
          expect(response.status).to.be.equal(200);
          done();
        });
    });

    it("failed to update po details", function () {
      chai
        .request(Router)
        .patch("/updatePoDetails/:id")
        .end((error, response) => {
          expect(response.status).to.be.equal(400);
          done();
        });
    });
  });

  describe("update status api", function () {
    it("update po status", function () {
      chai
        .request(Router)
        .patch("/updatePoStatus/:id")
        .end((error, response) => {
          expect(response.status).to.be.equal(200);
          done();
        });
    });

    it("failed to update po status", function () {
      chai
        .request(Router)
        .patch("/updatePoStatus/:id")
        .end((error, response) => {
          expect(response.status).to.be.equal(400);
          done();
        });
    });
  });
});
