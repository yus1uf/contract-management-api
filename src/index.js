require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const swaggerJsDocs = require("swagger-jsdoc")
const swaggerUI = require("swagger-ui-express")
const PORouters = require("./api/routs/PORoutes");
const SortingRouter = require("./api/routs/SortingRoutes")
const EmployeeRoutes = require("./api/routs/EmployeeRoutes")


const app = express();

const db = mongoose.connection;
const port = process.env.PORT || 3001;

const swaggerOption = {
  definition:{
    openapi:'3.0.0',
    info:{
      title:'Contract Management API',
      version:'1.0.0',
      description:'Contract Management API for VB-ERP System',
    },
    servers:[
      {
        url: "http://localhost:3000"
      },
    ],
  },
  apis:["./src/api/routs/*.js"]
}

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

const swaggerDocs = swaggerJsDocs(swaggerOption);
app.use('/api-doc',swaggerUI.serve,swaggerUI.setup(swaggerDocs))

app.get("/", (req, res) => {
  res.send("<h1>Welcome</h1>");
});

app.use(PORouters);
app.use(SortingRouter)
app.use(EmployeeRoutes)

app.listen(port, async () => {
  try {
    await mongoose.connect(process.env.DB_URL);
    db.on("error", () => console.error.bind(console, "Connection Error:"));
    db.once("open", function () {
      console.log("Connected sucssessfully");
    });
  } catch (error) {
    console.log("Something went wrong DB Connection failed");
  }

  console.log(`Server runing on port ${port}`);
});
