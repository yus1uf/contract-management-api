const SowService = require("../service/sowService");

module.exports = class SortingController extends SowService {
  constructor() {
    super();
  }
  sortProduct = async (req, res) => {
    try {
      const data = req.params.product;
      const productSorted = await this.sortSOW(data);
      res.status(201).send(productSorted);
    } catch (error) {
      res.status(401).send(error);
    }
  };
};
