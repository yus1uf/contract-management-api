const Joi = require("joi")
const schema = Joi.object({
  PO_Id: Joi.string().required(),
    Employee_Name: Joi.string().required(),
    Employee_Id: Joi.string().required(),
    Allocation_Rate:Joi.number().required(),
    Start_Date: Joi.date(),
    End_Date: Joi.date()
});

const EmployeeValidation = async (req, res, next) => {
    try {
      const result = schema.validate(req.body);
      if (result.error) {
        res.status(422).json(result.error);
      } else {
        next();
      }
    } catch (error) {
      res.status(400).send(result.error);
    }
  };
  module.exports = { EmployeeValidation };