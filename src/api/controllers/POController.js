const SowService = require("../service/sowService");
module.exports = class POController extends SowService {
  constructor() {
    super();
  }

  storePODetails = async (req, res) => {
    try {
      const storeDetails = await this.callStoreDetails(req.body);
      res.status(201).json(storeDetails);
    } catch (error) {
      res.status(400).send("Something Went Wrong");
    }
  };

  getPODetais = async (req, res) => {
    try {
      const projectDetails = await this.details();
      res.status(200).json(projectDetails);
    } catch (error) {
      res.status(400).send("Something went wrong");
    }
  };

  getDetailsById = async (req, res) => {
    try {
      const idDetails = await this.callGetDetailsById(req.params.id);
      res.status(200).json(idDetails);
    } catch (error) {
      res.status(400).send("Something went wrong");
    }
  };

  updatePODetais = async (req, res) => {
    try {
      const updateDetails = await this.callUpdateDetails(
        req.params.id,
        req.body
      );
      res.status(200).json(updateDetails);
    } catch (error) {
      res.status(400).send("Something went wrong");
    }
  };

  updatePOStatus = async (req, res) => {
    try {
      const updateDetails = await this.callUpdateStatus(req.params.id);
      res.status(200).json(updateDetails);
    } catch (error) {
      res.status(400).send("Something went wrong");
    }
  };
};
