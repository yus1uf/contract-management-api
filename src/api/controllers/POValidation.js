const Joi = require("joi");
const schema = Joi.object({
  Client_Name: Joi.string().min(3).max(30).lowercase().required(),
  Project_Name: Joi.string().required(),
  Client_Sponser: Joi.array()
    .items(Joi.string().alphanum().lowercase().required())
    .required(),
  Client_Finance_Controller: Joi.array()
    .items(Joi.string().lowercase().required())
    .required(),
  Targetted_Resources: Joi.array().items(Joi.string().required()).required(),
  Status: Joi.string()
    .valid("Rejected", "Pending", "Accepted", "Closed", "Drafted")
    .required(),
  Type: Joi.string().alphanum().valid("PO", "SOW").required(),
  PO_Number: Joi.string().alphanum().required(),
  PO_Amount: Joi.number().required(),
  Currency: Joi.string().alphanum().required(),
  Document_Name: Joi.string().required(),
  Document_Type: Joi.string().required(),
  Remarks: Joi.string(),
});
const POValidation = async (req, res, next) => {
  try {
    const result = schema.validate(req.body);
    if (result.error) {
      // console.log("hii");
      res.status(422).json(result.error);
    } else {
      // console.log("hello");
      next();
    }
  } catch (error) {
    res.status(400).send(result.error);
  }
};
module.exports = { POValidation };
