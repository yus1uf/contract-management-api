const EmployeeService = require("../service/EmployeeService")

module.exports = class EmployeeController extends EmployeeService{
    constructor(){
        super()
    }
    storeEmployee = async (req,res) => {
        try {
            const savedEmpDetails = await this.storeService(req.body)
             res.status(201).send(savedEmpDetails)
        } catch (error) {
             res.status(404).send("Some Error Occured")
        }
    }

    getEmployee = async (req,res) => {
        try {
            const Details=await this.getService(req.params.id)
            res.status(201).send(Details)
        } catch (error) {
            res.status(404).send("Some Error Occured")
        }
    }

    updateAssigneeDetails = async (req,res) => {
        try {
            const updatedDetails = await this.updateService(req.params.id,req.body)
            res.status(201).send(updatedDetails)
        } catch (error) {
            res.status(401).send("Unable to update Record");
        }
    }

    unassign = async (req,res) => {
        try {
            const anassignDetails = await this.unassignService(req.params.id)
            res.status(201).send(anassignDetails)
        } catch (error) {
            res.status(401).send("Unable to unassign Employee");
        }
    }
}