const express = require("express");
const Router = express.Router();

const SortingController = require("../controllers/SortingController")
const sortingObject = new SortingController()

Router.get("/sort/:product", sortingObject.sortProduct)

module.exports = Router;