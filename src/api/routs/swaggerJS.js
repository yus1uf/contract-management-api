/**
 * @swagger
 * definitions:
 *  purchaseOrderModel:
 *     type: object
 *     properties:
 *      Client_Name:
 *       type:string
 *      Project_Name:
 *       type:string
 *      Client_Sponser:
 *       type:[]
 *      Client_Finance_Controller:
 *       type:[]
 *      Targetted_Resources:
 *       type:[]
 *      Status:
 *       type:string
 *      Type:
 *       type:string
 *      PO_Number:
 *       type:string
 *      PO_Amount:
 *       type:number
 *      Currency:
 *       type:string
 *      Document_Name:
 *       type:string
 *      Document_Type:
 *       type:string
 *      Remarks:
 *       type:string
 *     example:
 *      Client_Name: Yusuf Shekh
 *      Project_Name: Ysvd23
 *      Client_Sponser: ['RCB','SHR']
 *      Client_Finance_Controller: ['DSC']
 *      Targetted_Resources: ['QWC']
 *      Status: Accepted
 *      Type: PO
 *      PO_Number: ysr234
 *      PO_Amount: 23243
 *      Currency: USD
 *      Document_Name: yOuser
 *      Document_Type: .pdf
 *      Remarks: skdghkdsfhdfhdlfd
 */

/**
 * @swagger
 * /savePoDetails:
 *  post:
 *   summary: Create Record
 *   description: create PO/SOW records
 *   requestBody:
 *    content:
 *     application/json:
 *      schema:
 *       $ref: '#/definitions/purchaseOrderModel'
 *   responses:
 *    201:
 *     description: Record Created
 *     content:
 *      application/json:
 *       schema:
 *        $ref: '#/definitions/purchaseOrderModel'
 *    400:
 *     description: Failure in creating record
 */

/**
 * @swagger
 * /getPoDetails:
 *  get:
 *   summary: Get All Records
 *   description: Get the list of all records
 *   responses:
 *    201:
 *     description: All Record Retrived
 *     content:
 *      application/json:
 *      schema:
 *       $ref: '#/definitions/purchaseOrderModel'
 *    400:
 *     description: Failure in fetching records
 */

/**
 * @swagger
 * /getPoDetailsByID/{id}:
 *  get:
 *   summary: Get Records of ID
 *   description: Get the record of specific ID
 *   parameters:
 *    - in: path
 *      name: id
 *      schema:
 *       type: string
 *       example: 618bb4ac58ff61d30d16e74f
 *   responses:
 *    201:
 *     description: Specific ID record retrive
 *     content:
 *      application/json:
 *      schema:
 *       $ref: '#/definitions/purchaseOrderModel'
 *    400:
 *     description: Failure in fetching records
 */

/**
 * @swagger
 * /sort/{product}:
 *  get:
 *   summary: Sorting Records
 *   description: Sort the Recoeds based on Specific Key
 *   parameters:
 *    - in: path
 *      name: product
 *      schema:
 *       type: string
 *       example: Client_Name
 *   responses:
 *    201:
 *     description: Record Sorted Based on Parameter
 *     content:
 *      application/json:
 *       schema:
 *        $ref: '#/definitions/purchaseOrderModel'
 *    400:
 *     description: Failure in creating record
 */
/**
 * @swagger
 * /updatePoDetails/{id}:
 *  patch:
 *   summary: Updating Record
 *   description: Update the Recoeds based on Specific ID
 *   parameters:
 *    - in: path
 *      name: id
 *      schema:
 *       type: string
 *       example: 618bb4ac58ff61d30d16e74f
 *   requestBody:
 *    description: Record Updated Based on Parameter
 *    content:
 *     application/json:
 *      schema:
 *       $ref: '#/definitions/purchaseOrderModel'
 *   responses:
 *    201:
 *     description: Record Updated Successfully
 *    400:
 *     description: Failure in updating record
 */

