const express = require("express")

const Router = express.Router();
const {EmployeeValidation} = require("../controllers/EmployeeValidation")
const EmployeeController = require("../controllers/EmployeeController")
var EmpObject = new EmployeeController();

Router.post("/addAssignee",EmployeeValidation,EmpObject.storeEmployee)
Router.patch("/updateDetails/:id",EmployeeValidation,EmpObject.updateAssigneeDetails)
Router.patch("/unassign/:id",EmpObject.unassign)
Router.get("/getAssignEmployee/:id",EmpObject.getEmployee)

module.exports = Router;