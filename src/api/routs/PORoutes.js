const express = require("express");

const Router = express.Router();
const { POValidation } = require("../controllers/POValidation");

const POController = require("../controllers/POController");
const PoObject = new POController();

Router.post("/savePoDetails", POValidation, PoObject.storePODetails);
Router.get("/getPoDetails", PoObject.getPODetais);
Router.get("/getPoDetailsByID/:id", PoObject.getDetailsById);
Router.patch("/updatePoDetails/:id", POValidation, PoObject.updatePODetais);
Router.patch("/updatePoStatus/:id", PoObject.updatePOStatus);

module.exports = Router;
