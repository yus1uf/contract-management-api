var SowCrud = require("../DAO/sowCrud");
module.exports = class SowService extends SowCrud {
  constructor() {
    super();
  }

  async sortSOW(param) {
    return await this.sort(param);
  }

  async callStoreDetails(param) {
    return await this.storeDetails(param);
  }

  async callUpdateDetails(param, body) {
    return await this.updatePoDetails(param, body);
  }

  async details() {
    return await this.getPoDetails();
  }

  async callGetDetailsById(param) {
    return await this.getPODetailsByID(param);
  }
  async callUpdateStatus(param) {
    return await this.updatePoStatus(param);
  }
};
