const EmployeeCrud = require("../DAO/EmployeeCrud")

module.exports = class EmployeeService extends EmployeeCrud{
    constructor(){
        super();
    }
    async storeService(param){
        return await this.storeAssignee(param)
    }

    async getService(param){
        return await this.getDetails(param)
    }

    async updateService(param,body){
        return await this.updateDetails(param,body)
    }

    async unassignService(param){
        var endDate = new Date().toDateString();
        return await this.unassignEmployee(param,endDate);
    }
}