const EmployeesModel = require("../models/EmployeesModel")

module.exports = class EmployeeCrud{
    constructor() {}

    async storeAssignee(param){
        const saveAssignee = new EmployeesModel(
           {
             PO_Id:param.PO_Id,
             Employee_Id:param.Employee_Id,
            Employee_Name: param.Employee_Name,
            Status: "assign",
            Allocation_Rate: param.Allocation_Rate,
            Start_Date: param.Start_Date,
            End_Date: param.End_Date
        }
        )
        saveAssignee.save()
        return saveAssignee;
    }

    async getDetails(param){
        const details = EmployeesModel.find({PO_Id:param})
        return details;
    }

    async updateDetails(param,body){
    
        const updateDetails = EmployeesModel.updateOne(
            {Employee_Id: param},
            {
                $set: {
                    Allocation_Rate: body.Allocation_Rate,
                    Start_Date: body.Start_Date,
                    End_Date: body.End_Date
                }
            }
        );
        return updateDetails;
    }

    async unassignEmployee(param,endDate){
        const updateEndDate = EmployeesModel.updateOne(
            {Employee_Id: param},
            {
                $set: {
                    Status: "unassign",
                    End_Date: endDate
                }
            }
        );
        return updateEndDate;
    }
}