const purchaseOrderModel = require("../models/POModels");

module.exports = class SowCrud {
  constructor() {}

  async sort(param) {
    try {
      const productSorted = await purchaseOrderModel.find({}).sort(param);
      return productSorted;
    } catch (error) {
      return error;
    }
  }

  async storeDetails(param) {
    try {
      const saveDetail = new purchaseOrderModel({
        Client_Name: param.Client_Name,
        Project_Name: param.Project_Name,
        Client_Sponser: param.Client_Sponser,
        Client_Finance_Controller: param.Client_Finance_Controller,
        Targetted_Resources: param.Targetted_Resources,
        Status: param.Status,
        Type: param.Type,
        PO_Number: param.PO_Number,
        PO_Amount: param.PO_Amount,
        Currency: param.Currency,
        Document_Name: param.Document_Name,
        Document_Type: param.Document_Type,
        Remarks: param.Remarks,
      });
      saveDetail.save();
      return saveDetail;
    } catch (error) {
      return error;
    }
  }

  async getPoDetails() {
    try {
      const details = await purchaseOrderModel.find({});
      return details;
    } catch (error) {
      return error;
    }
  }

  async getPODetailsByID(param) {
    try {
      var query = { _id: param };
      const getDetails = await purchaseOrderModel.findOne(query);
      return getDetails;
    } catch (error) {
      return error;
    }
  }

  async updatePoDetails(param, body) {
    try {
      const updateDetails = await purchaseOrderModel.updateOne(
        { _id: param },
        {
          $set: {
            Client_Name: body.Client_Name,
            Project_Name: body.Project_Name,
            Client_Sponser: body.Client_Sponser,
            Client_Finance_Controller: body.Client_Finance_Controller,
            Targetted_Resources: body.Targetted_Resources,
            Type: body.Type,
            PO_Number: body.PO_Number,
            PO_Amount: body.PO_Amount,
            Currency: body.Currency,
            Document_Name: body.Document_Name,
            Document_Type: body.Document_Type,
            Remarks: body.Remarks,
          },
        }
      );
      return updateDetails;
    } catch (error) {
      return error;
    }
  }

  async updatePoStatus(param) {
    try {
      const id = { _id: param };
      const getDetails = await purchaseOrderModel.findById(id);

      const { Status } = getDetails;
      const status2 = "drafted";
      const newStatus = "Pending";
      if (Status.toLowerCase() === status2) {
        const updateDetails = await purchaseOrderModel.updateOne(
          { _id: param },
          {
            $set: {
              Status: newStatus,
            },
          }
        );
        return { message: "status updated successfully" };
      } else {
        return { message: "something went wrong" };
      }
    } catch (error) {
      return error;
    }
  }
};
